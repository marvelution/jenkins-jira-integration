/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.configuration;

import java.net.URI;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import org.marvelution.jji.JiraUtils;
import org.marvelution.jji.events.JobNotificationType;
import org.marvelution.jji.model.parsers.ParserProvider;
import org.marvelution.jji.synctoken.CanonicalHttpServletRequest;
import org.marvelution.jji.synctoken.SyncTokenBuilder;

import hudson.Extension;
import hudson.model.AbstractDescribableImpl;
import hudson.model.Item;
import hudson.model.Run;
import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;
import org.apache.commons.lang.StringUtils;
import org.kohsuke.stapler.DataBoundConstructor;
import org.kohsuke.stapler.DataBoundSetter;

import static java.util.Arrays.asList;
import static java.util.Optional.of;
import static org.apache.commons.lang.StringUtils.defaultIfBlank;
import static org.marvelution.jji.Headers.NOTIFICATION_TYPE;
import static org.marvelution.jji.JiraUtils.asJson;

public class JiraSite
        extends AbstractDescribableImpl<JiraSite>
{

    public static final MediaType JSON = MediaType.get("application/json; charset=utf-8");
    private final URI uri;
    private String identifier;
    private String sharedSecret;
    private String name;
    private boolean postJson;

    @DataBoundConstructor
    public JiraSite(URI uri)
    {
        this.uri = uri;
    }

    public URI getUri()
    {
        return uri;
    }

    public String getIdentifier()
    {
        return identifier;
    }

    @DataBoundSetter
    public void setIdentifier(String identifier)
    {
        this.identifier = identifier;
    }

    public JiraSite withIdentifier(String identifier)
    {
        this.identifier = identifier;
        return this;
    }

    public String getSharedSecret()
    {
        return sharedSecret;
    }

    @DataBoundSetter
    public void setSharedSecret(String sharedSecret)
    {
        this.sharedSecret = sharedSecret;
    }

    public JiraSite withSharedSecret(String sharedSecret)
    {
        this.sharedSecret = sharedSecret;
        return this;
    }

    public String getName()
    {
        return defaultIfBlank(name, "Jira");
    }

    @DataBoundSetter
    public void setName(String name)
    {
        this.name = name;
    }

    public JiraSite withName(String name)
    {
        this.name = name;
        return this;
    }

    public boolean isPostJson()
    {
        return postJson;
    }

    @DataBoundSetter
    public void setPostJson(boolean postJson)
    {
        this.postJson = postJson;
    }

    public JiraSite withPostJson(boolean postJson)
    {
        this.postJson = postJson;
        return this;
    }

    public Request createGetBaseUrlRequest()
    {
        return signRequest(new Request.Builder().get()
                .url(getHttpUrl("base-url"))
                .build());
    }

    public Request createGetIssueLinksRequest(
            String jobHash,
            int buildNumber)
    {
        return signRequest(new Request.Builder().get()
                .url(getHttpUrl("integration/" + jobHash + "/" + buildNumber + "/links"))
                .build());
    }

    public Request createRegisterRequest()
    {
        // force setting the Content-Length header on the request
        return signRequest(new Request.Builder().post(RequestBody.create("", JSON))
                .url(getHttpUrl("integration/register/" + identifier))
                .addHeader("Content-Length", "0")
                .build());
    }

    public Request createNotifyJobCreatedRequest(Item item)
    {
        return signRequest(new Request.Builder().url(getHttpUrl("integration/" + JiraUtils.getJobHash(item)))
                .header(NOTIFICATION_TYPE, JobNotificationType.JOB_CREATED.value())
                .post(RequestBody.create(asJson(item, asList("name", "url")), JSON))
                .build());
    }

    public Request createNotifyJobModifiedRequest(Item item)
    {
        return signRequest(new Request.Builder().url(getHttpUrl("integration/" + JiraUtils.getJobHash(item)))
                .header(NOTIFICATION_TYPE, JobNotificationType.JOB_MODIFIED.value())
                .post(RequestBody.create(asJson(item,
                        ParserProvider.jobParser()
                                .fields()), JSON))
                .build());
    }

    public Request createNotifyJobMovedRequest(
            String oldJobHash,
            Item newItem)
    {
        return signRequest(new Request.Builder().post(RequestBody.create(asJson(newItem,
                        ParserProvider.jobParser()
                                .fields()), JSON))
                .header(NOTIFICATION_TYPE, JobNotificationType.JOB_MOVED.value())
                .url(getHttpUrl("integration/" + oldJobHash))
                .build());
    }

    public Request createNotifyBuildCompleted(Run run)
    {
        Request.Builder request;
        if (isPostJson())
        {
            request = new Request.Builder().post(RequestBody.create(asJson(run,
                    ParserProvider.buildParser()
                            .fields()), JSON));
        }
        else
        {
            request = new Request.Builder().put(RequestBody.create(JiraUtils.getAllParentHashes(run)
                    .stream()
                    .collect(Collectors.joining("\",\"", "[\"", "\"]")), JSON));
        }

        return signRequest(request.url(getHttpUrl("integration/" + JiraUtils.getJobHash(run) + "/" + run.getNumber()))
                .build());
    }

    public Request createNotifyJobDeletedRequest(Item item)
    {
        return signRequest(new Request.Builder().delete()
                .url(getHttpUrl("integration/" + JiraUtils.getJobHash(item)))
                .build());
    }

    public Request createNotifyBuildDeletedRequest(Run run)
    {
        return signRequest(new Request.Builder().delete()
                .url(getHttpUrl("integration/" + JiraUtils.getJobHash(run) + "/" + run.getNumber()))
                .build());
    }

    private HttpUrl getHttpUrl(String uri)
    {
        return HttpUrl.get(this.uri)
                .newBuilder()
                .addPathSegments(uri)
                .build();
    }

    public Request signRequest(Request request)
    {
        Request.Builder builder = request.newBuilder();
        CanonicalHttpServletRequest canonicalHttpRequest = new CanonicalHttpServletRequest(request.method(),
                request.url()
                        .uri(),
                getContextPath());
        new SyncTokenBuilder().identifier(identifier)
                .sharedSecret(sharedSecret)
                .request(canonicalHttpRequest)
                .generateTokenAndAddHeaders(builder::addHeader);
        return builder.build();
    }

    private Optional<String> getContextPath()
    {
        return of(getUri().getPath()).map(path -> path.substring(0, path.indexOf("/rest/")))
                .filter(StringUtils::isNotBlank);
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }
        JiraSite jiraSite = (JiraSite) o;
        return postJson == jiraSite.postJson && Objects.equals(uri, jiraSite.uri) && Objects.equals(identifier, jiraSite.identifier) &&
               Objects.equals(sharedSecret, jiraSite.sharedSecret) && Objects.equals(name, jiraSite.name);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(uri, identifier, sharedSecret, name, postJson);
    }

    @Override
    public String toString()
    {
        return "Jira site " + getName() + " at " + uri;
    }

    @Extension
    public static class Descriptor
            extends hudson.model.Descriptor<JiraSite>
    {

        @Override
        public String getDisplayName()
        {
            return "Jira Site";
        }
    }
}
