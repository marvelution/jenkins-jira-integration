/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.configuration;

import java.net.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.logging.*;
import java.util.stream.*;

import hudson.*;
import jenkins.model.*;
import net.sf.json.*;
import org.jenkinsci.*;
import org.kohsuke.stapler.*;

@Symbol(JiraSitesConfiguration.ID)
@Extension
public class JiraSitesConfiguration
		extends GlobalConfiguration
{

	static final String ID = "jira-integration-configuration";
	private static final Logger LOGGER = Logger.getLogger(JiraSitesConfiguration.class.getName());
	private Set<JiraSite> sites = new CopyOnWriteArraySet<>();

	public JiraSitesConfiguration()
	{
		load();
	}

	public static JiraSitesConfiguration get()
	{
		return (JiraSitesConfiguration) Jenkins.get().getDescriptorOrDie(JiraSitesConfiguration.class);
	}

	@Override
	public boolean configure(
			StaplerRequest req,
			JSONObject json)
	{
		return true;
	}

	public Set<JiraSite> getSites()
	{
		return sites;
	}

	public void registerSite(JiraSite site)
	{
		if (sites.removeIf(existing -> existing.getUri().equals(site.getUri())))
		{
			LOGGER.log(Level.INFO, "Updating registration for Jira Site {0}", site);
		}
		else
		{
			LOGGER.log(Level.INFO, "Adding registration for Jira Site {0}", site);
		}
		sites.add(site);
		save();
	}

	public void unregisterSite(URI uri)
	{
		if (sites.removeIf(site -> site.getUri().equals(uri)))
		{
			save();
			LOGGER.log(Level.INFO, "Unregistered Jira Site at: {0}", uri);
		}
	}

	@DataBoundSetter
	public void setSites(Set<JiraSite> sites)
	{
		this.sites = sites;
	}

	@Override
	public String getId()
	{
		return ID;
	}

	@Override
	public String getDisplayName()
	{
		return "Jira Sites";
	}

	public Stream<JiraSite> stream()
	{
		return sites.stream();
	}
}
