/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.security;

import javax.servlet.http.*;

import org.marvelution.jji.configuration.*;

import com.nimbusds.jwt.*;
import hudson.security.*;
import org.acegisecurity.*;
import org.acegisecurity.context.*;

public class SyncTokenSecurityContext
		implements SecurityContext
{

	private static final String REQUEST_ATTRIBUTE = SyncTokenSecurityContext.class.getName();
	private final JWTClaimsSet claimsSet;
	private final JiraSite site;
	private Authentication authentication;

	public SyncTokenSecurityContext(
			JWTClaimsSet claimsSet,
			JiraSite site)
	{
		this.claimsSet = claimsSet;
		this.site = site;
		authentication = ACL.SYSTEM;
	}

	@Override
	public Authentication getAuthentication()
	{
		return authentication;
	}

	@Override
	public void setAuthentication(Authentication authentication)
	{
		this.authentication = authentication;
	}

	public JWTClaimsSet getClaimsSet()
	{
		return claimsSet;
	}

	public JiraSite getSite()
	{
		return site;
	}

	void attachToRequest(HttpServletRequest request)
	{
		request.setAttribute(REQUEST_ATTRIBUTE, this);
	}

	void detachFromRequest(HttpServletRequest request)
	{
		request.removeAttribute(REQUEST_ATTRIBUTE);
	}

	public static SyncTokenSecurityContext checkSyncTokenAuthentication(HttpServletRequest request)
	{
		SecurityContext context = SecurityContextHolder.getContext();
		// JEP-227 was implemented in 2.266 and this brakes this check.
		if (context instanceof SyncTokenSecurityContext)
		{
			return (SyncTokenSecurityContext) context;
		}
		Object syncTokenSecurityContext = request.getAttribute(REQUEST_ATTRIBUTE);
		if (syncTokenSecurityContext instanceof SyncTokenSecurityContext)
		{
			return (SyncTokenSecurityContext) syncTokenSecurityContext;
		}
		throw new AccessDeniedException("Sync Token authentication required");
	}
}
