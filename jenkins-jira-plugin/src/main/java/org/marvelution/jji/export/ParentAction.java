/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.export;

import hudson.model.*;
import org.kohsuke.stapler.export.*;

/**
 * {@link Action} to expose the {@link Item parent}.
 *
 * @author Mark Rekveld
 * @since 3.6.0
 */
@ExportedBean
public class ParentAction implements Action {

	private final Parent parent;

	public ParentAction(Item item) {
		parent = new Parent(item);
	}

	@Exported(visibility = 2)
	public Parent getParent() {
		return parent;
	}

	@Override
	public String getIconFileName() {
		return null;
	}

	@Override
	public String getDisplayName() {
		return null;
	}

	@Override
	public String getUrlName() {
		return null;
	}

}
