/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.export;

import java.util.*;
import javax.annotation.*;

import hudson.*;
import hudson.model.*;
import jenkins.model.*;

/**
 * {@link TransientActionFactory} implementation for adding a {@link ParentAction} to a {@link Run}.
 *
 * @author Mark Rekveld
 * @since 3.6.0
 */
@Extension
public class BuildParentTransientActionFactory extends TransientActionFactory {

	@Override
	public Class type() {
		return Run.class;
	}

	@Nonnull
	@Override
	public Collection<? extends Action> createFor(@Nonnull Object target) {
		if (target instanceof Run) {
			return Collections.singletonList(new ParentAction(((Run) target).getParent()));
		} else {
			return Collections.emptyList();
		}
	}
}
