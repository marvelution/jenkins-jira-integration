/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.listener;

import javax.inject.*;

import org.marvelution.jji.*;

import hudson.*;
import hudson.model.*;
import javaposse.jobdsl.plugin.*;

@Extension(optional = true)
public class JobDSLListener
		extends ContextExtensionPoint
{

	private SitesClient client;

	@Inject
	public void setClient(SitesClient client) {
		this.client = client;
	}

	@Override
	public void notifyItemCreated(
			Item item,
			DslEnvironment dslEnvironment)
	{
		client.notifyJobCreated(item);
	}

	@Override
	public void notifyItemUpdated(
			Item item,
			DslEnvironment dslEnvironment)
	{
		client.notifyJobModified(item);
	}
}
