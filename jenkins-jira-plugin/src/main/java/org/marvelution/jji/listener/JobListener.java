/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.listener;

import javax.inject.*;

import org.marvelution.jji.*;

import hudson.*;
import hudson.model.*;
import hudson.model.listeners.*;

@Extension
public class JobListener
		extends ItemListener
{

	private SitesClient client;

	@Inject
	public void setClient(SitesClient client)
	{
		this.client = client;
	}

	@Override
	public void onCreated(Item item)
	{
		client.notifyJobCreated(item);
	}

	@Override
	public void onUpdated(Item item)
	{
		client.notifyJobModified(item);
	}

	@Override
	public void onLocationChanged(
			Item item,
			String oldFullName,
			String newFullName)
	{
		String jobHash = JiraUtils.getJobHash(item);
		// Execute the notification in a separate thread so url generation doesn't look at the current request.
		new Thread(() -> client.notifyJobMoved(jobHash, item)).start();
	}

	@Override
	public void onDeleted(Item item)
	{
		client.notifyJobDeleted(item);
	}
}
