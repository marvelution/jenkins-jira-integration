/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.management;

import java.io.IOException;
import java.net.URI;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.servlet.ServletException;

import org.marvelution.jji.Headers;
import org.marvelution.jji.JiraPlugin;
import org.marvelution.jji.configuration.JiraSite;
import org.marvelution.jji.configuration.JiraSitesConfiguration;
import org.marvelution.jji.security.SyncTokenSecurityContext;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import hudson.Extension;
import hudson.model.ManagementLink;
import jenkins.model.Jenkins;
import net.sf.json.JSONObject;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import org.acegisecurity.AccessDeniedException;
import org.kohsuke.stapler.HttpResponses;
import org.kohsuke.stapler.StaplerRequest;
import org.kohsuke.stapler.StaplerResponse;
import org.kohsuke.stapler.bind.JavaScriptMethod;
import org.kohsuke.stapler.interceptor.RequirePOST;

import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.toCollection;
import static org.marvelution.jji.JiraUtils.getJsonFromRequest;
import static org.marvelution.jji.Messages.*;
import static org.marvelution.jji.synctoken.SyncTokenAuthenticator.OLD_SYNC_TOKEN_HEADER_NAME;

@Extension(ordinal = Integer.MAX_VALUE - 500)
public class JiraSiteManagement
        extends ManagementLink
{

    public static final String URL_NAME = "jji";
    private static final Logger LOGGER = Logger.getLogger(JiraSiteManagement.class.getName());
    private JiraSitesConfiguration sitesConfiguration;
    private OkHttpClient httpClient;
    private ObjectMapper objectMapper;
    private JiraSite site;

    @Inject
    public void setSitesConfiguration(JiraSitesConfiguration sitesConfiguration)
    {
        this.sitesConfiguration = sitesConfiguration;
    }

    @Inject
    public void setHttpClient(OkHttpClient httpClient)
    {
        this.httpClient = httpClient;
    }

    @Inject
    public void setObjectMapper(ObjectMapper objectMapper)
    {
        this.objectMapper = objectMapper;
    }

    @Override
    public String getIconFileName()
    {
        return "/plugin/" + JiraPlugin.SHORT_NAME + "/images/48x48/jji.png";
    }

    @Override
    public String getDisplayName()
    {
        return manage_display_name();
    }

    @Override
    public String getDescription()
    {
        return manage_description();
    }

    @Override
    public String getUrlName()
    {
        return URL_NAME;
    }

    public Set<JiraSite> getSites()
    {
        return sitesConfiguration.stream()
                .sorted(comparing(JiraSite::getName))
                .collect(toCollection(LinkedHashSet::new));
    }

    @JavaScriptMethod
    public void deleteSite(String uri)
    {
        sitesConfiguration.unregisterSite(URI.create(uri));
    }

    @JavaScriptMethod
    public String getSiteUrl(String url)
    {
        URI uri = URI.create(url);
        return sitesConfiguration.stream()
                .filter(site -> {
                    try
                    {
                        return site.getUri()
                                .equals(uri);
                    }
                    catch (Exception e)
                    {
                        throw HttpResponses.errorWithoutStack(500, site_get_url_failed());
                    }
                })
                .findFirst()
                .map(site -> {
                    try
                    {
                        Response response = httpClient.newCall(site.createGetBaseUrlRequest())
                                .execute();
                        return response.body()
                                .string();
                    }
                    catch (Exception e)
                    {
                        throw HttpResponses.errorWithoutStack(500, site_get_url_failed());
                    }
                })
                .orElseThrow(() -> HttpResponses.errorWithoutStack(404, site_not_found()));
    }

    public String getBaseHelpUrl()
    {
        return "/plugin/" + JiraPlugin.SHORT_NAME + "/help/";
    }

    public void doIndex(
            StaplerRequest req,
            StaplerResponse rsp)
            throws IOException, ServletException
    {
        Jenkins.get()
                .getACL()
                .checkPermission(Jenkins.ADMINISTER);

        if (req.hasParameter("url") && req.hasParameter("token"))
        {
            String url = req.getParameter("url");
            String token = req.getParameter("token");

            try (Response response = httpClient.newCall(new Request.Builder().get()
                            .addHeader(Headers.SYNC_TOKEN, token)
                            // Keep setting the old header for the time being
                            .addHeader(OLD_SYNC_TOKEN_HEADER_NAME, token)
                            .url(url)
                            .build())
                    .execute())
            {
                if (response.isSuccessful())
                {
                    try (ResponseBody body = response.body())
                    {
                        if (body != null)
                        {
                            JsonNode details = objectMapper.readTree(body.bytes());

                            site = new JiraSite(URI.create(details.get("url")
                                    .asText())).withIdentifier(details.get("identifier")
                                            .asText())
                                    .withName(details.get("name")
                                            .asText())
                                    .withSharedSecret(details.get("sharedSecret")
                                            .asText())
                                    .withPostJson(details.get("firewalled")
                                            .asBoolean());

                            req.getView(this, "add")
                                    .forward(req, rsp);
                        }
                    }
                }
                else
                {
                    req.setAttribute("error", "Unable to get registration details; " + response.code() + "[" + response.message() + "]");
                }
            }
        }
        else
        {
            req.getView(this, "index")
                    .forward(req, rsp);
        }
    }

    public JiraSite getSite()
    {
        return site;
    }

    @RequirePOST
    public synchronized void doSubmit(
            StaplerRequest req,
            StaplerResponse rsp)
            throws IOException, ServletException
    {
        Jenkins.get()
                .getACL()
                .checkPermission(Jenkins.ADMINISTER);
        JSONObject form = req.getSubmittedForm();
        try
        {
            JiraSite site = new JiraSite(URI.create(form.getString("url"))).withIdentifier(form.getString("identifier"))
                    .withName(form.getString("name"))
                    .withSharedSecret(form.getString("sharedSecret"))
                    .withPostJson(form.getBoolean("firewalled"));

            try (Response response = httpClient.newCall(site.createRegisterRequest())
                    .execute())
            {
                if (response.code() != 202)
                {
                    throw new IllegalStateException(
                            "Failed to complete the registration with " + site + "; " + response.code() + "[" + response.message() + "]");
                }
            }

            sitesConfiguration.registerSite(site);

            rsp.sendRedirect(".");
        }
        catch (Exception e)
        {
            LOGGER.log(Level.SEVERE, "Registration of Jira site failed", e);
            req.setAttribute("error", e.getMessage());
            req.getView(this, "index")
                    .forward(req, rsp);
        }
    }

    @RequirePOST
    public void doRegister(StaplerRequest request)
            throws IOException
    {
        SyncTokenSecurityContext securityContext = SyncTokenSecurityContext.checkSyncTokenAuthentication(request);
        Jenkins.get()
                .getACL()
                .checkPermission(Jenkins.ADMINISTER);

        JSONObject data = getJsonFromRequest(request);
        JiraSite jiraSite = new JiraSite(URI.create(data.getString("url"))).withName(data.getString("name"))
                .withIdentifier(data.getString("identifier"))
                .withSharedSecret(data.getString("sharedSecret"))
                .withPostJson(data.optBoolean("firewalled", false));

        JiraSite existingSite = securityContext.getSite();
        if (existingSite != null && Objects.equals(jiraSite.getIdentifier(), existingSite.getIdentifier()) &&
            Objects.equals(jiraSite.getSharedSecret(), existingSite.getSharedSecret()))
        {
            JiraSitesConfiguration.get()
                    .registerSite(jiraSite);
        }
        else if (Objects.equals(jiraSite.getIdentifier(),
                securityContext.getClaimsSet()
                        .getIssuer()))
        {
            JiraSitesConfiguration.get()
                    .registerSite(jiraSite);
        }
        else
        {
            throw new AccessDeniedException("Unauthorized Jira site registration attempt.");
        }
    }

    @RequirePOST
    @SuppressWarnings("lgtm[jenkins/no-permission-check]")
    public void doUnregister(StaplerRequest request)
            throws IOException
    {
        SyncTokenSecurityContext.checkSyncTokenAuthentication(request);
        JiraSitesConfiguration.get()
                .unregisterSite(URI.create(getJsonFromRequest(request).getString("url")));
    }
}
