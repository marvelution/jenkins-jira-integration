/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.casc;

import java.net.*;
import java.util.*;
import javax.annotation.*;

import hudson.*;
import io.jenkins.plugins.casc.*;
import io.jenkins.plugins.casc.model.*;

@Extension(optional = true)
public class URIConfigurator
		implements Configurator<URI>
{

	@Override
	public Class<URI> getTarget()
	{
		return URI.class;
	}

	@Nonnull
	@Override
	public Set<Attribute<URI, ?>> describe()
	{
		return Collections.emptySet();
	}

	@Nonnull
	@Override
	public URI configure(
			CNode config,
			ConfigurationContext context)
			throws ConfiguratorException
	{
		return URI.create(config.asScalar().getValue());
	}

	@Override
	public URI check(
			CNode config,
			ConfigurationContext context)
			throws ConfiguratorException
	{
		return configure(config, context);
	}

	@Override
	public CNode describe(
			URI instance,
			ConfigurationContext context)
	{
		return new Scalar(instance.toASCIIString());
	}
}
