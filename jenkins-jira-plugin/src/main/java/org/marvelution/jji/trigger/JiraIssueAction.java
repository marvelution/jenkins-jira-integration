/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.trigger;

import hudson.model.*;

import static org.marvelution.jji.JiraPlugin.*;

public class JiraIssueAction
		implements Action
{

	private String issueUrl;
	private String issueKey;

	public JiraIssueAction(
			String issueUrl,
			String issueKey)
	{
		this.issueUrl = issueUrl;
		this.issueKey = issueKey;
	}

	@Override
	public String getIconFileName()
	{
		return "/plugin/" + SHORT_NAME + "/images/24x24/jji.png";
	}

	@Override
	public String getDisplayName()
	{
		return issueKey;
	}

	@Override
	public String getUrlName()
	{
		return issueUrl;
	}
}
