/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.trigger;

import org.marvelution.jji.Messages;

import hudson.model.Cause;
import org.kohsuke.stapler.export.Exported;

/**
 * {@link Cause} used when a build is triggered through Jira.
 *
 * @author Mark Rekveld
 * @since 3.3.0
 */
public class JiraCause extends Cause {

	private String issueKey;
	private String by;

	public JiraCause(String issueKey, String by) {
		this.issueKey = issueKey;
		this.by = by;
	}

	@Override
	public String getShortDescription() {
		return Messages.cause_triggered_through(by, issueKey);
	}

	@Exported(visibility = 3)
	public String getIssueKey() {
		return issueKey;
	}

	@Exported(visibility = 3)
	public String getBy() {
		return by;
	}
}
