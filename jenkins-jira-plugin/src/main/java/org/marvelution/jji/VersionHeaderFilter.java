/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji;

import java.io.*;
import java.util.logging.*;
import javax.servlet.Filter;
import javax.servlet.*;
import javax.servlet.http.*;

import hudson.*;
import hudson.init.*;
import hudson.util.*;
import jenkins.model.*;

import static org.marvelution.jji.JiraPlugin.*;

public class VersionHeaderFilter
		implements Filter
{

	private static final Logger LOGGER = Logger.getLogger(VersionHeaderFilter.class.getName());

	@Initializer(after = InitMilestone.PLUGINS_STARTED)
	public static void registerFilter()
	{
		VersionHeaderFilter filter = new VersionHeaderFilter();
		if (!PluginServletFilter.hasFilter(filter))
		{
			try
			{
				PluginServletFilter.addFilter(filter);
			}
			catch (ServletException e)
			{
				LOGGER.log(Level.WARNING, "Failed to set up version header servlet filter", e);
			}
		}
	}

	@Override
	public void init(FilterConfig filterConfig)
			throws ServletException
	{}

	@Override
	public void doFilter(
			ServletRequest request,
			ServletResponse response,
			FilterChain chain)
			throws IOException, ServletException
	{
		if (response instanceof HttpServletResponse)
		{
			Plugin plugin = Jenkins.get().getPlugin(SHORT_NAME);
			if (plugin == null)
			{
				LOGGER.log(Level.WARNING, "Unable to locate plugin " + SHORT_NAME);
			}
			else
			{
				((HttpServletResponse) response).setHeader(VERSION_HEADER, plugin.getWrapper().getVersion());
			}
		}
		chain.doFilter(request, response);
	}

	@Override
	public void destroy()
	{}
}
