/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.rest;

import java.time.*;

import org.marvelution.jji.*;

import com.google.inject.*;
import io.jenkins.plugins.okhttp.api.*;
import okhttp3.*;

public class HttpClientProvider
{

	private final OkHttpClient httpClient;

	public HttpClientProvider()
	{
		httpClient = JenkinsOkHttpClient.newClientBuilder(new OkHttpClient())
				.connectTimeout(Duration.ofMillis(5000))
				.readTimeout(Duration.ofMillis(5000))
				.writeTimeout(Duration.ofMillis(5000))
				.addInterceptor(chain -> {
					Request originalRequest = chain.request();
					Request userAgentRequest = originalRequest.newBuilder().header("User-Agent", JiraPlugin.SHORT_NAME).build();
					return chain.proceed(userAgentRequest);
				})
				.build();
	}

	@Provides
	public OkHttpClient httpClient()
	{
		return httpClient;
	}
}
