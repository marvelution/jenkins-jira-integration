/*
 * Copyright (c) 2012-present Marvelution Holding B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function () {
    if (window.jQuery === window.$$) {
        // jQuery is used as framework.
        $$(document).ajaxError(function (event, jqXhr) {
            alert(jqXhr.statusText);
        });
    } else {
        // Assume Prototype is the framework used, like the JavaScriptMethod binding does.
        Ajax.Responders.register({
            onComplete: function (response) {
                if (!response.success()) {
                    alert(response.transport.statusText);
                }
            }
        });
    }

    $$('tr[data-site-uri] a').each(function (element) {
        element.onclick = function () {
            let row = this.up('tr');
            let url = row.readAttribute('data-site-uri');

            if (this.hasClassName('remove-site')) {
                JJI.deleteSite(url, function () {
                    if (row.up('tbody').childElements('tr').length === 1) {
                        window.location.reload();
                    } else {
                        row.remove();
                    }
                });
            } else if (this.hasClassName('navigate-to-site')) {
                JJI.getSiteUrl(url, function (response) {
                    if (confirm('Navigate to: ' + response.responseJSON)) {
                        window.location = response.responseJSON;
                    }
                });
            }
        };
    });
})();
